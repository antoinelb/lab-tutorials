# How to use git and GitHub/GitLab

## Getting started

If this is a new project,

- Open your command line
- Go to the root of the project (cd)
- Run `git init`

If you want to start start from an existing project,

- Open your command line
- Go to where you want the project to be (cd)
- Run `git clone url-of-the-project`

## Writing and commiting files

In git, files may be either unchanged, staged or unstaged. Unchanged files are
self explanatory, so let's look at the difference between staged and unchanged.

Using the command

`git status`

it's possible to see the current status of staged and unstaged files

It's possible to add files one at a time with the command

`git add file1 file2 ...`

or all at once with

`git add -A`

Sometimes there are files you'll never want to keep track such as whatever is
node_modules/ or \_\_pycache\_\_. Any file or directory to ignore has to be
put in a text file at the root of the project called `.gitignore`. Each line
will be a file or directory to ignore. Git understands the wildcard \*
representing any number of characters.

To make git save these staged files, you need to commit them. For this, you use
the command

`git commit -m 'informative message describing the commit'`

This message should describe at least generally what was changed during that
commit.

## Branches

When you create or clone a project, you automatically start on the `master`
branch. This is the trunk of your project to which all created branches will
eventually be joined.

Each branch contains its own version of the code that you can modify without
affecting the other branches. To see all branches, you use the command

`git branch`

To create a new branch, the command is

`git branch branch_name`

This creates a new branch containing a copy of the code of the current branch
you're in. But after creating it, you're still on the old branch. To switch to
this new branch, you use the command

`git checkout branch_name`

The two previous steps can be combined in a single one with the command

`git checkout -b branch_name`

You'll typically use a branch to implement a specific thing without breaking
the previous code. But once this thing is done, you'll want to put it on the
`master` branch. To do this, there are 3 steps:

- Go to the `master` branch using `git checkout master`
- Merge the code on your branch with the code in the `master one` using `git merge branch_name`
- (Optionally) Delete your old branch with `git branch -d branch_name` (prevents clutering)
