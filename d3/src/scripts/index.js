"use strict";

/*********/
/* model */
/*********/
let model;

function initModel(skills, skillCooccurence) {
  model = {
    skills: skills,
    skillCooccurence: skillCooccurence,
  };
}

async function readData(dataset) {
  const resp = await fetch(`data/${dataset}.json`);
  return await resp.json();
}

/**********/
/* update */
/**********/

function update() {}

function ticked(nodes, links, context, width, height, colourScale) {
  context.clearRect(0, 0, width, height);

  links.forEach(d => drawLink(context, d));

  nodes.forEach(d => drawNode(context, d, colourScale));
}

function dragSubject(simulation, canvas) {
  const x = (d3.event.x / canvas.clientWidth) * canvas.width;
  const y = (d3.event.y / canvas.clientHeight) * canvas.height;
  return simulation.find(x, y);
}

function dragStarted(simulation) {
  if (!d3.event.active) {
    simulation.alphaTarget(0.3).restart();
  }
  d3.event.subject.fx = d3.event.subject.x;
  d3.event.subject.fy = d3.event.subject.y;
}

function dragged() {
  d3.event.subject.fx = d3.event.x;
  d3.event.subject.fy = d3.event.y;
}

function dragEnded(simulation) {
  if (!d3.event.active) {
    simulation.alphaTarget(0);
  }
  d3.event.subject.fx = null;
  d3.event.subject.fy = null;
}

function mouseMove(simulation, canvas) {
  const x = (d3.event.layerX / canvas.clientWidth) * canvas.width;
  const y = (d3.event.layerY / canvas.clientHeight) * canvas.height;

  const node = simulation.find(x, y, 10);

  if (node) {
    d3.select("#tooltip")
      .style("opacity", 0.8)
      .style("top", d3.event.pageY + 5 + "px")
      .style("left", d3.event.pageX + 5 + "px")
      .html(node.skill_name);
  } else {
    d3.select("#tooltip").style("opacity", 0);
  }
}

/********/
/* view */
/********/

function view() {
  skillGraphView();
}

function skillGraphView() {
  const width = 1000;
  const height = 1000;

  const colourScale = d3
    .scaleLinear()
    .domain([0, 1])
    .range(["#b30000", "#339966"]);

  const canvas = d3
    .select("#graph")
    .attr("width", width)
    .attr("height", height);

  const tooltip = document.createElement("div");
  tooltip.id = "tooltip";
  document.querySelector("body").appendChild(tooltip);

  const context = canvas.node().getContext("2d");

  const nodes = model.skills.map(s => s);
  let links = model.skillCooccurence
    .filter(s => s.count)
    .map(s => ({
      source: s.skill1.skill_id,
      target: s.skill2.skill_id,
      strength: s.count,
    }));
  links = links.map(l => ({
    ...l,
    ...{ strength: l.strength / Math.max(...links.map(ll => ll.strength)) },
  }));

  const simulation = d3
    .forceSimulation()
    .force("link", d3.forceLink().id(d => d.skill_id))
    .force("charge", d3.forceManyBody().strength(-100))
    .force("x", d3.forceX(width / 2).strength(0.1))
    .force("y", d3.forceY(height / 2).strength(0.1));

  simulation
    .nodes(nodes)
    .on("tick", () =>
      ticked(nodes, links, context, width, height, colourScale),
    );

  simulation.force("link").links(links);

  canvas.on("mousemove", () => mouseMove(simulation, canvas.node()));

  canvas.call(
    d3
      .drag()
      .container(canvas.node())
      .subject(() => dragSubject(simulation, canvas.node()))
      .on("start", () => dragStarted(simulation))
      .on("drag", dragged)
      .on("end", () => dragEnded(simulation)),
  );
}

function drawLink(context, d) {
  context.beginPath();
  context.moveTo(d.source.x, d.source.y);
  context.lineTo(d.target.x, d.target.y);
  context.lineWidth = 10 * d.strength;
  context.strokeStyle = "#e9def7";
  context.stroke();
  context.closePath();
}

function drawNode(context, d, colourScale) {
  context.beginPath();
  context.moveTo(d.x, d.y);
  context.arc(d.x, d.y, 5, 0, 2 * Math.PI);
  context.fillStyle = colourScale(d.correct / (d.correct + d.incorrect));
  context.fill();
  context.closePath();
}

/********/
/* init */
/********/

async function init() {
  const skills = await readData("skills");
  const skillCooccurence = await readData("skill_cooccurence");

  initModel(skills, skillCooccurence);
  update();
  view();
}

window.onload = init;
