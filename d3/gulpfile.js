const autoprefixer = require("autoprefixer");
const babel = require("rollup-plugin-babel");
const browserSync = require("browser-sync").create();
const buffer = require("vinyl-buffer");
const commonjs = require("rollup-plugin-commonjs");
const concat = require("gulp-concat");
const gulp = require("gulp");
const postcss = require("gulp-postcss");
const resolve = require("rollup-plugin-node-resolve");
const rollup = require("rollup-stream");
const sass = require("gulp-sass");
const source = require("vinyl-source-stream");
const sourcemaps = require("gulp-sourcemaps");
const { eslint } = require("rollup-plugin-eslint");
const { uglify } = require("rollup-plugin-uglify");

const babelConfig = {
  presets: [
    [
      "@babel/env",
      {
        targets: {
          browsers: ["last 3 versions"],
        },
        modules: false,
      },
    ],
  ],
  plugins: [
    "@babel/plugin-proposal-object-rest-spread",
    [
      "@babel/plugin-transform-runtime",
      {
        helpers: false,
        regenerator: true,
      },
    ],
  ],
  exclude: "node_modules/**",
  babelrc: false,
};

function index() {
  return gulp.src("./src/index.html").pipe(gulp.dest("./dist"));
}

async function styles() {
  return gulp
    .src("./src/styles/*.scss")
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        outputStyle: "compressed",
        includePaths: "./node_modules",
      }),
    )
    .pipe(
      postcss([
        autoprefixer({
          browsers: ["last 3 versions"],
        }),
      ]),
    )
    .pipe(concat("styles.css"))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("./dist"))
    .pipe(browserSync.stream());
}

async function scripts() {
  return rollup({
    input: "./src/scripts/index.js",
    sourcemap: true,
    format: "iife",
    name: "scripts",
    globals: {
      d3: "d3",
    },
    external: ["d3"],
    plugins: [
      eslint({
        exclude: ["**.css"],
      }),
      babel(babelConfig),
      resolve({
        jsnext: true,
        main: true,
        browser: true,
      }),
      commonjs(),
      uglify(),
    ],
  })
    .pipe(source("scripts.js"))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("./dist"))
    .pipe(browserSync.stream());
}

function data() {
  return gulp.src("./data/*").pipe(gulp.dest("./dist/data"));
}

function watch() {
  browserSync.init({
    server: "./dist",
  });
  gulp.watch("./src/scripts/*.js", scripts);
  gulp.watch("./src/styles/*.scss", styles);
  gulp.watch("./src/index.html", index);
  gulp.watch("./dist/index.html").on("change", browserSync.reload);
}

const build = gulp.parallel(index, styles, scripts, data);
const dev = gulp.series(build, watch);

exports.build = build;
exports.dev = dev;
