#!/bin/python
import os
from itertools import groupby
from operator import itemgetter

import ujson


def read_data():
    path = os.path.join(
        os.path.dirname(__file__), os.pardir, "data", "data.csv"
    )
    with open(path, "r", encoding="latin1") as f:
        cols = f.readline().strip().split(",")
        data = [
            {col: val for col, val in zip(cols, line.strip().split(","))}
            for line in f
        ]
    return data


def create_skills(data):
    path_skills = os.path.join(
        os.path.dirname(__file__), os.pardir, "data", "skills.json"
    )
    path_skill_cooccurence = os.path.join(
        os.path.dirname(__file__), os.pardir, "data", "skill_cooccurence.json"
    )
    grouped = [
        {
            "skill_id": group[0]["skill_id"],
            "skill_name": group[0]["skill_name"],
            "problems": {g["problem_id"] for g in group},
            "correct": sum(g["correct"] == "1" for g in group),
            "incorrect": sum(g["correct"] == "0" for g in group),
        }
        for group in (
            list(group)
            for _, group in groupby(
                sorted(data, key=itemgetter("skill_id")),
                key=itemgetter("skill_id"),
            )
        )
        if group[0]["skill_id"]
    ]
    cooccurences = [
        {
            "skill1": skill1,
            "skill2": skill2,
            "count": len(set(skill1["problems"]) & set(skill2["problems"])),
        }
        for i, skill1 in enumerate(grouped[:-1])
        for skill2 in grouped[i:]
    ]
    with open(path_skills, "w") as f:
        ujson.dump(grouped, f)
    with open(path_skill_cooccurence, "w") as f:
        ujson.dump(cooccurences, f)


def create_sequences(data):
    path = os.path.join(
        os.path.dirname(__file__), os.pardir, "data", "sequences.json"
    )
    grouped = [
        {
            "user_id": group[0]["user_id"],
            "sequence_id": group[0]["sequence_id"],
            "sequence": [
                {
                    "problem_id": problem["problem_id"],
                    "skill_id": problem["skill_id"],
                    "skill_name": problem["skill_name"],
                    "position": problem["position"],
                    "correct": problem["correct"],
                }
                for problem in group
            ],
        }
        for group in (
            list(group)
            for _, group in groupby(
                sorted(data, key=itemgetter("user_id", "sequence_id")),
                key=itemgetter("user_id", "sequence_id"),
            )
        )
    ]
    with open(path, "w") as f:
        ujson.dump(grouped, f)


if __name__ == "__main__":
    data = read_data()
    #  create_grouped_by_question(data)
    #  create_sequences(data)
    create_skills(data)
